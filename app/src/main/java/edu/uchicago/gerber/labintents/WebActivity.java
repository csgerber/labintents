package edu.uchicago.gerber.labintents;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.EditText;

public class WebActivity extends AppCompatActivity {

    private EditText editText;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        editText = findViewById(R.id.editText);
        webView = findViewById(R.id.webView);

        Uri uri = getIntent().getData();
        editText.setText(uri.toString());
        webView.loadUrl(uri.toString());
    }
}
