package edu.uchicago.gerber.labintents;

import java.io.Serializable;

/**
 * Created by macbook on 4/13/17.
 */

public class Model implements Serializable {

    //////////////////////////////////////////////////////
    //                 members
    //////////////////////////////////////////////////////

    private int age;
    private String name;

    //////////////////////////////////////////////////////
    //                 constcutors
    //////////////////////////////////////////////////////

    public Model(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //////////////////////////////////////////////////////
    //                 getters/setters
    //////////////////////////////////////////////////////

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
