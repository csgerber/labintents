package edu.uchicago.gerber.labintents;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity2 extends AppCompatActivity {


    public static final String NAME = "NAME";
    private Button startButton;
    private EditText editName;
    private Button buttonGoogle;


    //we can send primitives
    //Strings
    //String arrays
    //primiitive arrays
    //Any Serialized object
    //Any Parcelable object




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = (Button) findViewById(R.id.startButton);
        editName = (EditText) findViewById(R.id.editName);
        //http://stackoverflow.com/questions/5596261/intent-filter-to-launch-my-activity-when-custom-uri-is-clicked
        buttonGoogle = (Button) findViewById(R.id.buttonGoogle);

        buttonGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent googleIntent = new Intent();
                googleIntent.setAction(Intent.ACTION_VIEW);
                googleIntent.setData(Uri.parse("https://www.google.com/"));
                startActivity(googleIntent);

            }
        });



        editName.setText(getIntent().getExtras() == null ? "": getIntent().getExtras().get(NAME).toString());


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent intent = new Intent(MainActivity.this, SecondActivity.class);

//                Bundle bundle = new Bundle();
//                bundle.putString(NAME, editName.getText().toString());
//                Model model = new Model("James", 55);
//                bundle.putSerializable("SERIALIZE", model);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT,"Your score and Some extra text");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "The title");
                startActivity(Intent.createChooser(shareIntent, "Share..."));
                //intent.putExtras(bundle);

                startActivity(shareIntent);

                finish();


            }
        });

    }
}
