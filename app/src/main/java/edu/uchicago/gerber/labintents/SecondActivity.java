package edu.uchicago.gerber.labintents;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

public class SecondActivity extends AppCompatActivity {


    public static final String NAME = "NAME";
    private Button startButton;
    private EditText editName;
    private Button buttonSms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        startButton = (Button) findViewById(R.id.startButton);
        editName = (EditText) findViewById(R.id.editName);

        buttonSms = (Button) findViewById(R.id.buttonSms);


        editName.setText(getIntent().getExtras() == null ? "" : getModel());


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, MainActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString(NAME, editName.getText().toString());

                intent.putExtras(bundle);

                startActivity(intent);

                finish();


            }
        });


        buttonSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //http://stackoverflow.com/questions/9798657/send-sms-via-intent
                Uri uri = Uri.parse("smsto:" + editName.getText().toString());
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("sms_body", "Wow this actually works!");
                startActivity(it);
            }
        });


        //http://stackoverflow.com/questions/28420242/getting-url-with-intent-android
       // Uri uri = this.getIntent().getData();

      //  Toast.makeText(SecondActivity.this, uri.getHost(), Toast.LENGTH_LONG).show();


    }

    private String getModel() {

        Serializable serializable = getIntent().getExtras().getSerializable("SERIALIZE");
        if (serializable != null && serializable instanceof Model) {

            Model model = (Model) serializable;

            String strValue = "";
            strValue += model.getName();
            strValue += " " + model.getAge();
            return strValue;

        } else {
            return "no date";
        }


    }
}
